##############################################################################
 INTRODUZIONE AL PROGETTO LEARNING-<Algorithms and Data Structures>
##############################################################################
Il progetto Learning-<Algorithms and Data Structures> mira a realizzare 
un manuale code oriented sull'implementazione dei principali algoritmi e delle
strutture dati fondamentali.

Per code oriented si intende anzitutto immediata operativita', nel senso che
non ci saranno pagine da sfogliare ma solo e soprattutto righe di codice da
leggere ed interpretare, commenti mirati inoltre saranno inseriti all'interno
del codice stesso laddove ce ne fosse bisogno.

I libri talvolta si perdono in inutili dettagli, tralasciando troppo spesso
il codice, L-<ASD> invece si pone l'obiettivo di fare il lavoro opposto, e
rendere al codice la giusta collocazione che merita, ossia di primo attore
indiscusso.

##############################################################################
 LEARNING-<ASD>
##############################################################################
Il presente repository riguarda l'implementazione dei principali algoritmi di 
ricerca e di ordinamento nonche' delle strutture dati fondamentali; per lo 
sviluppo e' stato adoperato il linguaggio C-ANSI (c99).

Il lavoro e' stato suddiviso in:
- Strutture dati;
- Algoritmi di ricerca;
- Algoritmi di ordinamento;

La bibliografia di riferimento:
- "Algoritmi in C", (testo principale, fonte d'ispirazione inesauribile)
  di R. Sedgewick, terza edizione;

- "Introduzione agli algoritmi e strutture dati", 
  di T.H. Cormen, C.E. Leiserson, R.L.Rivest, C. Stein, seconda edizione;

- "Strutture di dati e algoritmi",
  di P.Crescenzi, G.Gambosi, R. Grossi;

- "Programmazione nella pratica",
  di B.W. Kernighan, R. Pike.

##############################################################################
INFO
##############################################################################
Per commenti, info e quant'altro: <behemoth _at_ autistici _dot_ org >

Qualora si volesse comunicare mediante crittografia a chiave pubblica:          
KeyID = 0xE83F9FBB                                                  

La chiave pubblica e' reperibile in uno dei server adibiti alla gestione delle
chiavi pubbliche.

